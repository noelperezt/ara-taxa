<?php require 'datos.php'; ?>
<!DOCTYPE HTML>
<html">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ara Taxa</title>
    <link rel="icon" type="image/png" href="images/icons/araicon.ico"/>
    <meta http-equiv="refresh" content="500" />
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/estylos.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">  
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</head>
<body>

    <script src="code/highcharts.js"></script>
    <script src="code/modules/exporting.js"></script>
    <script src="code/modules/export-data.js"></script>
    
    <nav>
        <div class="container">
            <div class="column one">
            
                <ul class="contact_details">
                    <li class="phone"><i class="fas fa-phone"></i><a href="tel:(92)3021-4353"><p style="color: #97cc02;">(92) 3021-4353</p></a></li><li class="phone"><i class="fas fa-phone"></i><a href="tel:(92)9469-5783"><p style="color: #97cc02;">(92) 9469-5783</p></a></li>              </ul>
                
                <ul class="social">
                    <li class="facebook">
                        <a target="_blank" href="https://facebook.com/aracambioturismo" title="Facebook"><i class="fab fa-facebook-f" style="font-size: 18px;"></i></a></li>
                    <li class="instagram">
                        <a target="_blank" href="http://instagram.com/aracambioeturismo" title="Instagram"><i class="fab fa-instagram" style="font-size: 18px;"></i></a></li></ul>
            </div>
        </div>
    </nav>
    <div class="contenido">
        <div>
            <img src="images/logo_ara_cambio_turismo.png" class="image">    
        </div>

        <div class="texto">
           <p><h1>Taxas Ara Cambio: </h1><h2><?php echo number_format($monto,2,',','.'); ?> BSF</h2></p>
        </div>
    </div>

    <div id="container" style="min-width: 310px; 
                                    height: 400px; 
                                    margin-top:5%; 
                                    font-family: 'Julius Sans One', sans-serif;">
    </div>

    <!--GRAFICO NO TOCAR-->
    <script type="text/javascript">
        Highcharts.chart('container', {
            chart: {
                type: 'line'
            },
            title: {
                text: ' '
            },
            subtitle: {
                text: ' '
            },
            xAxis: {
                categories: [<?php 
                $conexion = new PDO('mysql:host=localhost;dbname=resultin_aracambio', 'root', '');
                $datos = $conexion->query('SELECT fecha FROM (select *from historicos order by 1 desc limit 7) t order by 1 asc');
                    foreach ($datos as $mostrar) {
                           $fecha = $mostrar['fecha'];
                           echo "'".$fecha ."'," ;}
                    
                     //echo $fecha ."</br>";?>]
            },
            yAxis: {
                title: {
                    text: 'Monto'
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name: 'Taxa',
                data:
                 [<?php 
                $conexion = new PDO('mysql:host=localhost;dbname=resultin_aracambio', 'root', '');
                $datos = $conexion->query('SELECT monto FROM (select *from historicos order by 1 desc limit 7) t order by 1 asc');

                foreach ($datos as $mostrar) {
                           $monto = $mostrar['monto'];
                           echo $monto ."," ;
                    }
            
         ?>]
            }]
        });
    </script>

    <div>

        <footer>
            <div class="col-md-6 col-sm-8 agileinfo-copyright">
            <p>© 2018 Result Solutions. All rights reserved | Design by <a href="http://result.inf.br/">Result Solutions</a>
            </p> 
            <a href="http://result.inf.br/"><img src="images/LOGO-RESULT favicon.png" id="imgfooter" title="Ir para Result Solutions" class="logofooter"></a>
            </div>
        </footer>
    </div>
    	</body>
    </html>
